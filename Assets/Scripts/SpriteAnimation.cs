using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAnimation : MonoBehaviour
{
    private Sprite[] animationSprites;
    private SpriteRenderer spriteRenderer;
    private int animationSpriteIndex;
    // Start is called before the first frame update
    void Start()
    {
        animationSprites = gameObject.GetComponent<CharacterBehavior>().animationSprites;
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        
        spriteRenderer.sprite = animationSprites[0];
        Debug.Log($"{spriteRenderer.sprite.name}");

        animationSpriteIndex = 0;

    }

    // Update is called once per frame
    void Update()
    {
        spriteRenderer.sprite = animationSprites[animationSpriteIndex];
    }
}
