using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public enum PlayerKind
{
    Srcerer,
    Knight,
    Assassin,
    Bard,
    Barbarian
}
public class CharacterBehavior : MonoBehaviour

{
    public string cname;
    public string csurname;
    public PlayerKind type;
    public float heightMultiplicator = 5f;
    public float speed = 50f;
    public float distance = 0f;
    public Sprite idle;
    public Sprite walk1;
    public Sprite walk2;
    public SpriteRenderer Sp;
    public float weight = 1f;
    public Sprite[] animationSprites;
    public int animationSpriteIndex;

    private int puntoDestino = 0;

    public float destinacionRetutrno;

    private bool onCouldown = false;

    public int timeToStop = 1000;

    private bool stopGrow;

    void Start()
    {
        Sp.sprite = idle;
        Debug.Log($"Hola {cname}");
        Debug.Log($"Tipus {type}, Surname {csurname}, Speed {speed}");

        animationSprites = gameObject.GetComponent<CharacterBehavior>().animationSprites;
        Sp = gameObject.GetComponent<SpriteRenderer>();

        Sp.sprite = animationSprites[0];
        Debug.Log($"{Sp.sprite.name}");

        animationSpriteIndex = 0;

    }

    // Update is called once per frame
    void Update()
    {

        animationSpriteIndex++;
        Sp.sprite = animationSprites[animationSpriteIndex];

        if (animationSpriteIndex == 3)
        {
            animationSpriteIndex = 0;
        }

        
        transform.Translate(Input.GetAxis("Horizontal") * speed * Time.deltaTime / weight, 0f, 0f);

        /*if (transform.position.x < distance)
        {
            transform.Translate(speed * Time.deltaTime, 0f, 0f);

        }*/

        Patrollar();
        /*if (Input.GetKeyDown("space") && !onCouldown)
        {
            print("space key was pressed");
            onCouldown = true;
            
                Vector3 characterScale = transform.localScale;
                characterScale.y += 1 * Time.deltaTime;
                characterScale.x += 1 * Time.deltaTime;
            
        }*/
        Vector3 characterScale = transform.localScale;
        if (Input.GetAxis("Vertical") > 0)
        {
            characterScale.y += 1 * Time.deltaTime;
            characterScale.x += 1 * Time.deltaTime;
        } else if (Input.GetAxis("Vertical") < 0)
        {
            characterScale.y -= 1 * Time.deltaTime;
            characterScale.x -= 1 * Time.deltaTime;
        }
        transform.localScale = characterScale;




        /*if(transform.position.x < Mathf.Abs(distance))
        {
            transform.Translate(speed * Time.deltaTime, 0f, 0f);
        }
        else if (transform.position.x >= Mathf.Abs(distance))
        {
            do
            {
                transform.Translate(-speed * Time.deltaTime, 0f, 0f);
            } while (transform.position.x !=0);
            
        }*/



        /*Vector3 characterScale = transform.localScale;

        if (Input.GetAxis("Vertical") > 0)
        {
            characterScale.y += 1 * Time.deltaTime;
            characterScale.x += 1 * Time.deltaTime;
        }

        if (Input.GetAxis("Horizontal") == 0)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = idle;
        }
        transform.localScale = characterScale;*/



        /*if (transform.position.y >= -10.28)
        {
            transform.Translate(0f, -0.05f, 0f);
        }*/

    }

    public void Patrollar()
    {
        if (puntoDestino == 0)
        {
            if (distance > 0)
            {
                Sp.flipX = true;
                if (transform.position.x < distance)
                {
                    transform.Translate(speed * Time.deltaTime / weight, 0f, 0f);
                }
                else if (transform.position.x >= distance)
                {
                    puntoDestino = 1;
                }
            }
            else if (distance < 0)
            {
                Sp.flipX = false;
                if (transform.position.x > distance)
                {
                    transform.Translate(-speed * Time.deltaTime / weight, 0f, 0f);
                }
                else if (transform.position.x <= distance)
                {
                    puntoDestino = 1;
                }
            }

        }
        else if (puntoDestino == 2)
        {
            if (distance > 0)
            {
                Sp.flipX = false;
                if (transform.position.x > destinacionRetutrno - distance)
                {
                    transform.Translate(-speed * Time.deltaTime / weight, 0f, 0f);
                }
                else if (transform.position.x <= destinacionRetutrno - distance)
                {
                    puntoDestino = 0;
                }
            }
            else if (distance < 0)
            {
                Sp.flipX = true;
                if (transform.position.x < destinacionRetutrno - distance)
                {
                    transform.Translate(speed * Time.deltaTime / weight, 0f, 0f);
                }
                else if (transform.position.x >= destinacionRetutrno - distance)
                {
                    puntoDestino = 0;
                }
            }
        }

        if (puntoDestino == 1)
        {
            destinacionRetutrno = transform.position.x;
            puntoDestino = 2;
        }
    }

    IEnumerator PowerOff()
    {
        yield return new WaitForSeconds(timeToStop);
    }


    void Awake()
    {

        QualitySettings.vSyncCount = 0;  // VSync must be disabled
        Application.targetFrameRate = 12;
    }
}

