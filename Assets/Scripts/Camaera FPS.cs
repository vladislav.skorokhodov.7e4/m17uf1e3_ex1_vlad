using UnityEngine;

public class Example
{
    void Start()
    {
        // Make the game run as fast as possible
        Application.targetFrameRate = 100;
    }
}                   